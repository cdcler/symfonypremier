<?php

namespace App\Controller;

use App\Entity\Personne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Routing\Annotation\Route;

class PersonneController extends AbstractController
{
    /**
     * @Route("/personne/new", name="personne_success")
     */
    public function insertPersonne(Request $request)
    {
        $personne = new Personne();

        $form = $this->createFormBuilder($personne)
        ->add('nom', TextType::class)
        ->add('prenom')
        ->add('annee')
        ->add('genre')
        ->add('emploi')
        ->add('creer', SubmitType::class, ['label' => 'Creer Personne'])
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $personne = $form->getData();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($personne);
        $entityManager->flush();

        return $this->redirectToRoute('personne_success');

    }


    return $this->render('personne/new.html.twig', [
        'form' => $form->createview(),
        ]);
    
    }
}