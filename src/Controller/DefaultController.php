<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Essai;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(): Response
    {
        return new Response('Accueil Cyril');
    }


    
/**
     * @Route("/lucky/number/{max}", defaults={"max":100}, name="lucky_number")  //CECI EST DU CODE !!!! C'est une annotation PHP
     *
     * Au final, cela donne l'url suivante: http://localhost:8000/lucky/number/100
     *
     */
    public function numberAction($max)
    {
        // génération d'un nombre aléatoire
        $number = mt_rand(0, 100);
        //ici on va chercher le template et on lui transmet la variable
        return $this->render('default/number.html.twig', [
            // pour fournir des variables au template
            // a gauche, le nom qui sera utilisé dans le template
            // a droite, la valeur
            'number' => $number
      ]);
    }

/**
    * @Route(
    *    "/blog/{_locale}/{year}/{title}", 
    *    name="route_de_base_blog", 
    *
    *   defaults={"_locale": "fr"},
    *    requirements={
    *           "_locale": "fr|en",
    *           "year": "^\d{4}$",
    *           "title": "\w+"
    *    }
    *)
    */    
    public function artisteAction($_locale, $year, $title)
    {

        return $this->render('default/blog.html.twig', [
 
            '_locale' => $_locale,
            'title' => $title,    
            'year' => $year

       ]);
    }

/**
    * @Route("/layout", name="layout")
    */
    public function layout()
    {
        return $this->render('default/layout.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

 

    /**
     * @Route("/jobs", name="jobs_index")
     */
    public function listArticleAction()
    {
        $em = $this->getDoctrine()->getManager();
        $jobs = $em->getRepository(Essai::class)->findAll();

        dump($jobs);

        return $this->render('job/index.html.twig', [
            'jobs' => $jobs,
        ]);
    }

    /**
     * @Route("/job/{id}", name="job_show")
     */
    public function showArticleAction($id)
    {
        $job = $this->getDoctrine()
            ->getRepository(Essai::class)
            ->find($id);
    
        if (!$job) {
            throw $this->createNotFoundException(
                'Aucun article trouvé '.$id
            );
        }
    
        
        return $this->render('job/show.html.twig', ['job' => $job]);
    }

}